package model;/* created by David Tetour in November of 2017. */

import static dt.model.LoanRequest.setupUrl;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.junit.Test;

public class LoanRequestTest {

	@Test
	public void urlShouldComposeCorrectly() {
		String address = "address.com/for/testing";
		ZonedDateTime testTime = ZonedDateTime.of(
				LocalDate.of(2017, 11, 11),
				LocalTime.of(7, 7, 7, 100_000_000),
				ZoneId.of("+1"));
		String correctUrl = address
				+ "?datePublished__gt=2017-11-01T07%3A07%3A07.100%2B01%3A00"
				+ "&datePublished__lte=2017-11-11T07%3A07%3A07.100%2B01%3A00";
		assertEquals(setupUrl(address, testTime.minusDays(10), testTime).toString(), correctUrl);
	}
}
