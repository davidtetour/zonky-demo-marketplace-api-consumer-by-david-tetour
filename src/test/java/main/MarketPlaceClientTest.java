/* created by David Tetour in November of 2017. */

package main;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.Test;

import dt.main.MarketplaceClient;
import dt.model.LoanPage;

public class MarketPlaceClientTest {

	@Test
	public void closedRequestShouldResultInEmptyLoanPage() {
		MarketplaceClient testClient = new MarketplaceClient() {{
			setAddress("nonexistent.zonky.address.cz");
		}};
		assertEquals(testClient.requestLoanPage(), Optional.<LoanPage>empty());
	}
}
