/* created by David Tetour in November of 2017. */

package main;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Duration;
import java.util.Optional;

import org.junit.Test;

import dt.main.App;
import dt.main.MarketplaceClient;
import dt.model.Loan;
import dt.model.LoanPage;

public class AppTest {

	private Loan testLoan = new Loan(
			"2016-05-11T05:21:58.071-11:00",
			"id1",
			"url.com",
			"loan1",
			111,
			10000);

	private MarketplaceClient testClient = new MarketplaceClient() {
		@Override
		public Optional<LoanPage> requestLoanPage() {
			return Optional.of(new LoanPage(6, testLoan, testLoan));
		}
	};

	private App testApp = new App() {
		{
			setPeriod(Duration.ofMillis(1));
			setInitialDelay(Duration.ZERO);
			setClient(testClient);
		}
		@Override
		public void run() {
			collectLoans();
		}
	};

	/**
	 * running test app will invoke single {@link App#collectLoans()},
	 * and its recursive page requesting is tested on a client with 6 total loans
	 * available for request and fixed page size of 2. <hr/>
	 */
	@Test
	public void multipleLoanPagesAreRecursivelyCollected() {
		testApp.run();
		assertEquals(testApp.getLoans().size(), 6);
	}
}
