/* created by David Tetour in November of 2017. */

import java.time.Duration;
import java.time.ZonedDateTime;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import dt.main.App;
import dt.main.MarketplaceClient;
import main.AppTest;
import main.MarketPlaceClientTest;
import model.LoanRequestTest;
import util.JSONTest;

@RunWith(Suite.class)
@SuiteClasses({
		JSONTest.class,
		LoanRequestTest.class,
		MarketPlaceClientTest.class,
		AppTest.class
})

public class TestSuite {

	public static void main(String[] args) {
		MarketplaceClient testClient = new MarketplaceClient() {{
			setSinceTime(ZonedDateTime.now().minus(Duration.ofDays(10)));
		}};
		App testApp = new App() {{
			setPeriod(Duration.ofSeconds(10));
			setInitialDelay(Duration.ZERO);
			setClient(testClient);
		}};
		testApp.run();
	}
}


