package util;/* created by David Tetour in November of 2017. */

import static dt.util.JSON.utility;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

import dt.model.Loan;

public class JSONTest {

	@Test
	public void loanShouldStayEqualAfterSerializationAndDeserialization() {
		Loan testLoan = new Loan(
				"2016-05-11T05:21:58.071-11:00",
				"id1",
				"url.com",
				"loan1",
				111,
				10000);
		String mockLoanSerialized = utility.toJson(testLoan);
		Loan mockLoanDeSerialized = utility.fromJson(mockLoanSerialized, Loan.class);
		assertEquals(testLoan, mockLoanDeSerialized);
	}
}
