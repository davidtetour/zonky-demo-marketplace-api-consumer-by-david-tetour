/* created by David Tetour in November of 2017. */

package dt.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public interface JSON {

	/** preferred JSON serialization and deserialization utility tool */
	Gson utility = new GsonBuilder().setPrettyPrinting().create();
}
