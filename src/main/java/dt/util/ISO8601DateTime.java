/* created by David Tetour in November of 2017. */

package dt.util;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public interface ISO8601DateTime {

	/** syntax: {'+'|'-'}{'hh:mm'} examples: 'Z', '+01:00' or '-11:00' */
	String ZONE_OFFSET = "XXX";

	/** slightly stricter than ISO_OFFSET_DATE_TIME regarding zone offset format */
	String DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS" + ZONE_OFFSET;

	/** fulfills ISO-8601 */
	DateTimeFormatter ISO_DATE_TIME = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);

	/** @return ISO-8601 formatted date-time string */
	static String formatted(ZonedDateTime dateTime) {
		return dateTime.format(ISO_DATE_TIME);
	}
}
