/* created by David Tetour in October of 2017. */

package dt.main;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.time.Duration;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.apache.log4j.Logger;

import dt.model.Loan;
import dt.model.LoanPage;

/** zonky marketplace REST API consumer periodically requesting new loans */
public class App {

	private static final Logger logger = Logger.getLogger(App.class);
	/** used as a temporary storage alternative */
	private static final Logger loanLogger = Logger.getLogger(Loan.class);

	/** period of time after which requests are triggered */
	private Duration period = Duration.ofMinutes(5);
	/** initial delay after which the first new loans are printed */
	private Duration initialDelay = period;
	/** REST client communicating with loan marketplace */
	private MarketplaceClient client = new MarketplaceClient();
	/** new loans provided by the marketplace */
	private ArrayList<Loan> loans = new ArrayList<>();

	/** creates a new instance of the class and invokes {@link #run()} */
	public static void main(String[] args) {
		new App().run();
	}

	/** schedules {@link #printNewLoans()} task at a fixed frequency */
	public void run() {
		ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
		long period = this.period.toMillis();
		long initialDelay = this.initialDelay.toMillis();
		service.scheduleAtFixedRate(this::printNewLoans, initialDelay, period, MILLISECONDS);
	}

	/** repeatedly requests loans until all are collected, then prints them all out */
	protected void printNewLoans() {
		client.markPeriod();
		collectLoans();
		printLoansToFile();
		loans.clear();
	}

	/** recursively requests available loan pages for a defined interval and appends them to 'loans' */
	protected  void collectLoans() {
		LoanPage page = client.requestLoanPage().orElseThrow(IllegalStateException::new);
		loans.ensureCapacity(page.getTotalLoans());
		loans.addAll(page.getLoans());
		logger.info("collected " + loans.size() + " of " + page.getTotalLoans() + " new loans");
		if(page.getTotalLoans() > loans .size()) {
			client.incrementPage();
			collectLoans();
		}
	}

	/** crudely prints loans to a text file, temporary storage */
	protected void printLoansToFile() {
		loans.forEach(loanLogger::info);
	}

	/*-------------------- setters --------------------*/

	protected void setClient(MarketplaceClient client) {
		this.client = client;
	}

	protected void setPeriod(Duration period) {
		this.period = period;
	}

	protected void setInitialDelay(Duration initialDelay) {
		this.initialDelay = initialDelay;
	}

	/*-------------------- getters --------------------*/

	public ArrayList<Loan> getLoans() {
		return loans;
	}
}