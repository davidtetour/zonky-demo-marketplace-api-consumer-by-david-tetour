/* created by David Tetour in November of 2017. */

package dt.main;

import static dt.model.LoanRequest.setupHeaders;
import static dt.model.LoanRequest.setupUrl;
import static dt.model.Marketplace.ADDRESS;
import static dt.model.Marketplace.HeaderField.TOTAL;
import static dt.util.JSON.utility;
import static java.lang.Integer.parseInt;

import java.time.ZonedDateTime;
import java.util.Optional;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import dt.model.Loan;
import dt.model.LoanPage;

public class MarketplaceClient {

	private static final Logger logger = Logger.getLogger(MarketplaceClient.class);

	private ResteasyClient restClient = new ResteasyClientBuilder().build();
	private String address = ADDRESS;
	private String responseType = "*/*";
	private int pageSize = 100;
	private int pageNumber;
	private ZonedDateTime sinceTime = ZonedDateTime.now();
	private ZonedDateTime untilTime;

	public void markPeriod() {
		sinceTime = untilTime == null ? sinceTime : untilTime;
		untilTime = ZonedDateTime.now();
		pageNumber = 0;
	}

	public void incrementPage() {
		pageNumber++;
	}

	public Optional<LoanPage> requestLoanPage() {
		try (Response response = sendRequest()) {
			int totalLoans = parseInt(response.getStringHeaders().get(TOTAL.toString()).get(0));
			Loan[] loans = utility.fromJson(response.readEntity(String.class), Loan[].class);
			return Optional.of(new LoanPage(totalLoans, loans));
		} catch (ProcessingException e) {
			logger.warn("loan request did not result in successful response", e);
			return Optional.empty();
		}
	}

	protected Response sendRequest() {
		if (untilTime == null) {
			untilTime = ZonedDateTime.now();
			logger.warn("all loans until now are requested, use markPeriod() to set specific time period");
		}
		ResteasyWebTarget target = restClient.target(setupUrl(address, sinceTime, untilTime));
		Invocation.Builder builder = target.request(responseType);
		builder.headers(setupHeaders(pageNumber, pageSize));
		return builder.get();
	}

	/*-------------------- setters --------------------*/

	protected void setRestClient(ResteasyClient restClient) {
		this.restClient = restClient;
	}

	protected void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	protected void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	protected void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	protected void setSinceTime(ZonedDateTime sinceTime) {
		this.sinceTime = sinceTime;
	}

	protected void setUntilTime(ZonedDateTime untilTime) {
		this.untilTime = untilTime;
	}

	protected void setAddress(String address) {
		this.address = address;
	}
}
