/* created by David Tetour in November of 2017. */

package dt.model;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;

import dt.util.JSON;

/** simplified loan model with encapsulated fields */
public class Loan implements Serializable {

	private final String datePublished;
	private final String id;
	private final String url;
	private final String name;
	private final int userId;
	private final int amount;

	public Loan(String datePublished, String id, String url, String name, int userId, int amount) {
		this.datePublished = datePublished;
		this.id = id;
		this.url = url;
		this.name = name;
		this.userId = userId;
		this.amount = amount;
	}

	@Override
	public String toString() {
		return JSON.utility.toJson(this);
	}

	@Override
	public boolean equals(Object obj) {
		return obj.getClass() == getClass() && EqualsBuilder.reflectionEquals(this, obj);
	}

	/*-------------------- getters --------------------*/

	public String getDatePublished() {
		return datePublished;
	}

	public String getId() {
		return id;
	}

	public String getUrl() {
		return url;
	}

	public String getName() {
		return name;
	}

	public int getUserId() {
		return userId;
	}

	public int getAmount() {
		return amount;
	}
}

