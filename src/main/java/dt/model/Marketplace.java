/* created by David Tetour in November of 2017. */

package dt.model;

import static com.google.common.base.CaseFormat.LOWER_CAMEL;
import static com.google.common.base.CaseFormat.LOWER_HYPHEN;
import static com.google.common.base.CaseFormat.UPPER_UNDERSCORE;

public interface Marketplace {

	String ADDRESS = "https://api.zonky.cz/loans/marketplace";

	static String toQueryField(QueryFieldName name, QueryFieldOperator operator) {
		return name + "__" + operator;
	}

	enum HeaderField {
		ORDER, PAGE, SIZE, TOTAL;

		@Override
		public String toString() {
			return "x-" + UPPER_UNDERSCORE.to(LOWER_HYPHEN, super.toString());
		}
	}

	/** field operators are written in <strong>lowercase shorthand form</strong>*/
	enum QueryFieldOperator {
		GREATER_THAN("gt"),
		LESS_THAN_OR_EQUAL("lte");

		public final String shorthand;

		QueryFieldOperator(String shorthand) {
			this.shorthand = shorthand.toLowerCase();
		}

		@Override
		public String toString() {
			return shorthand;
		}
	}

	/** field names are written in <strong>lower camel case</strong>*/
	enum QueryFieldName {
		DATE_PUBLISHED;

		@Override
		public String toString() {
			return UPPER_UNDERSCORE.to(LOWER_CAMEL, super.toString());
		}
	}
}