/* created by David Tetour in November of 2017. */

package dt.model;

import java.util.Arrays;
import java.util.List;

/** simplified loan page from http marketplace get response content */
public class LoanPage {

	private final List<Loan> loans;
	private final int totalLoans;

	public LoanPage(int totalLoans, Loan... loans) {
		this.loans = Arrays.asList(loans);
		this.totalLoans = totalLoans;
	}

	/*-------------------- getters --------------------*/

	public List<Loan> getLoans() {
		return loans;
	}

	public int getTotalLoans() {
		return totalLoans;
	}
}

