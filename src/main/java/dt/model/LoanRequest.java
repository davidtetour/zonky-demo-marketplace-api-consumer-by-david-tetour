/* created by David Tetour in November of 2017. */

package dt.model;

import static dt.model.Marketplace.HeaderField.ORDER;
import static dt.model.Marketplace.HeaderField.PAGE;
import static dt.model.Marketplace.HeaderField.SIZE;
import static dt.model.Marketplace.QueryFieldName.DATE_PUBLISHED;
import static dt.model.Marketplace.QueryFieldOperator.GREATER_THAN;
import static dt.model.Marketplace.QueryFieldOperator.LESS_THAN_OR_EQUAL;
import static dt.model.Marketplace.toQueryField;
import static dt.util.ISO8601DateTime.formatted;

import java.net.URI;
import java.time.ZonedDateTime;

import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.UriBuilder;

public interface LoanRequest {

	/** builds url with parameters requesting all published loans within a given time period */
	static URI setupUrl(String address, ZonedDateTime sinceTime, ZonedDateTime toTime) {
		return UriBuilder
				.fromUri(address)
				.queryParam(toQueryField(DATE_PUBLISHED, GREATER_THAN), formatted(sinceTime))
				.queryParam(toQueryField(DATE_PUBLISHED, LESS_THAN_OR_EQUAL), formatted(toTime))
				.build();
	}

	/** builds http headers requesting a specific loan page sorted by loan publication date */
	static MultivaluedHashMap<String, Object> setupHeaders(int pageNumber, int pageSize) {
		return new MultivaluedHashMap<String, Object>()
		{{
			add(ORDER.toString(), DATE_PUBLISHED);
			add(SIZE.toString(), pageSize);
			add(PAGE.toString(), pageNumber);
		}};
	}
}
