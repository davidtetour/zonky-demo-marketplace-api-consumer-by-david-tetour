# README #
latest executable jar available in downloads
runnable with command:

> java -jar zonky-demo-marketplace-api-consumer-0.0.3-jar-with-dependencies.jar 

to build an executable jar from sources execute:

> mvn clean compile assembly:single


# Objective #
consuming JSON loan data provided by REST API at https://api.zonky.cz/loans/marketplace

# Structure #
Maven Java 1.8.065 console app relying on JBoss RESTEasy Client

# Versions and Changes #
0.0.1

minimalistic, single file implementation

0.0.2

support for paged requests for a scenario with large amount of newly published loans

correction of query parameter *datePublished* value to ensure that no loans are omitted in between requests

HTTP interface introduced to provide more formal composition of HTTP headers and HTTP query parameters

0.0.3

completely rewritten class structure, application and its main components are instantiable for ease of testing

log4j logging introduced, loans printed to a separate file

jUnit testsuite introduced with several unit tests to prevent future software regressions